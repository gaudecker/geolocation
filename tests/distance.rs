extern crate geolocation;

use geolocation::Location;

#[test]
fn distance() {
    let new_york = Location::from_coordinates(40.7127, -74.0059);
    let helsinki = Location::from_coordinates(60.1708, 24.9375);
    let munich = Location::from_coordinates(48.1333, 11.5667);

    assert_eq!(new_york.distance_to(&helsinki).round(), 6618.337890486902f64.round());
    assert_eq!(munich.distance_to(&helsinki).round(), 1590.1646151045206f64.round());
}
