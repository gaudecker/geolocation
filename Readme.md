# geolocation [![Build Status](https://travis-ci.org/gaudecker/geolocation.svg)](https://travis-ci.org/gaudecker/geolocation)

A library that lets you determine headings and distances of geographic locations.

## Documentation

The documentation is hosted on [rust-ci.org](http://www.rust-ci.org/gaudecker/geolocation/doc/geolocation/).
