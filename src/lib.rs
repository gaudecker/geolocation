//! A library that lets you determine headings and distances of
//! geographic locations.

// The Earth's radius in kilometers.
static EARTH_RADIUS: f64 = 6371.0;

/// A geographic location.
#[derive(Clone, Copy)]
pub struct Location {
    /// Latitude in degrees.
    pub latitude: f64,
    /// Longitude in degrees.
    pub longitude: f64
}

impl Location {
    /// Creates a new `Location` with latitude and longitude set to
    /// zero.
    ///
    /// # Example
    ///
    /// ```
    /// let l = geolocation::Location::new();
    /// assert_eq!(l.latitude, 0.0);
    /// assert_eq!(l.longitude, 0.0);
    /// ```
    pub fn new() -> Location {
        Location {
            latitude: 0.0,
            longitude: 0.0
        }
    }

    /// Creates a new `Location` with `latitude` and `longitude`.
    ///
    /// # Example
    ///
    /// ```
    /// let l = geolocation::Location::from_coordinates(48.1333, 11.5667);
    /// assert_eq!(l.latitude, 48.1333);
    /// assert_eq!(l.longitude, 11.5667);
    /// ```
    pub fn from_coordinates(latitude: f64, longitude: f64) -> Location {
        Location {
            latitude: latitude,
            longitude: longitude
        }
    }

    /// Returns the distance between `self` and `other` in meters. The
    /// calculation is done using the Haversine formula.
    ///
    /// # Example
    ///
    /// ```
    /// let new_york = geolocation::Location::from_coordinates(40.7127, -74.0059);
    /// let helsinki = geolocation::Location::from_coordinates(60.1708, 24.9375);
    /// 
    /// let dist = new_york.distance_to(&helsinki);
    /// ```
    pub fn distance_to(&self, other: &Location) -> f64 {
        let lat1 = self.latitude.to_radians();
        let lat2 = other.latitude.to_radians();
        let dlat = (other.latitude - self.latitude).to_radians();
        let dlon = (other.longitude - self.longitude).to_radians();

        let a = (dlat / 2.0).sin() * (dlat / 2.0).sin() +
            lat1.cos() * lat2.cos() *
            (dlon / 2.0).sin() * (dlon / 2.0).sin();
        let c = 2.0 * a.sqrt().atan2((1.0 - a).sqrt());

        EARTH_RADIUS * c
    }
}
